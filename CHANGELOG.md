# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased : 1.0.X]
### Added
  - optimisations :
    - https://www.alsacreations.com/astuce/lire/1811-chargement-images-differe-loading-lazy.html
    - https://www.alsacreations.com/astuce/lire/1562-script-attribut-async-defer.html
    - https://www.alsacreations.com/article/lire/1741-Optimisez-vos-polices-web.html
