"use strict";

function generate() {
    const promises = [];
    promises.push(fetch('static/components/modal/component.js').then(response => {
        return response.text().then(value => {
            return {
                'name': 'script.js',
                'value': value
            };
        });
    }));
    promises.push(fetch('static/components/modal/style.css').then(response => {
        return response.text().then(value => {
            return {
                'name': 'static.css',
                'value': value
            };
        });
    }));
    Promise.all(promises).then((results) => {
        generate_zip(results);
    });
}

function generate_zip(results) {
    let _zip = new JSZip();
     
    _zip.file("component.html", generate_html().innerHTML);

    let _static = _zip.folder("static");
    results.forEach(result => {
        _static.file(result.name, result.value);
    });
    _zip.generateAsync({type:"blob"}).then(function(content) {
        // see FileSaver.js
        saveAs(content, "modal.zip");
    });
}
