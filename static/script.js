"use strict";

const ASIDE_CONTAINER = getClassEl('aside__container');
const ASIDE_COMPONENT = getClassEl('aside__component');
const COMPONENT_CONTAINER = getClassEl('preview__container');
const COMPONENT_COMPONENT = getClassEl('preview__component');

function showHide(component, value_exist) {
    if (value_exist) {
        component.classList.remove('hidden');
        return;
    }
    component.classList.add('hidden');
}

function getClassEl(class_name) {
    return document.getElementsByClassName(class_name)[0];
}

function generate_html() {
    let componentNode = COMPONENT_CONTAINER.cloneNode(true);
    let hiddenElements = componentNode.querySelectorAll('.hidden');
    for(var i=0; i < hiddenElements.length; i++) {
        let hiddenElement = hiddenElements[i];
        hiddenElement.parentNode.removeChild(hiddenElement);
    };
    return componentNode;
}

function dragSeparator(element) {
    let side_x = event.x /window.innerWidth * 100;
    if (side_x < 20)
        side_x = 20;
    if (side_x > 80)
        side_x = 80;
    ASIDE_CONTAINER.style.width = (side_x - 0.5) + "%";
    COMPONENT_CONTAINER.style.width = (99.5 - side_x) + "%";
}

function render(element, properties) {
    let template = element.cloneNode(true);
    template.id = '';
    template.classList.remove('hidden');
    let _inner_html = template.innerHTML;
    Object.entries(properties).map(function (entry) {
        let key = entry[0];
        let value =  entry[1];
        _inner_html = _inner_html.replace('{{ ' + key + ' }}', value);
    });
    return _inner_html;
}
