"use strict";

export let CONTAINER = null;
export let CONTENT = null;
export let TITLE = null;
export let BODY = null;
export let CLOSE = null;
export let FOOTER = null;
export let FOOTER_BUTTON_TEMPLATE = null;

export let SIDE_TITLE = null;
export let SIDE_CONTENT = null;
export let SIDE_CLOSE_BUTTON = null;
export let SIDE_TEMPLATE_BUTTON = null;
export let SIDE_BUTTONS = null;

export function init() {
    CONTAINER = getClassEl('component-modal__container');
    CONTENT = document.getElementById('component-modal-content');
    TITLE = getClassEl('component-modal__title');
    BODY = getClassEl('component-modal__body');
    CLOSE = getClassEl('component-modal__close');
    FOOTER = getClassEl('component-modal__footer');
    FOOTER_BUTTON_TEMPLATE = document.getElementById('compoent-footer-template-button');

    SIDE_TITLE = document.getElementById('side-title');
    SIDE_CONTENT = document.getElementById('side-content');
    SIDE_CLOSE_BUTTON = document.getElementById('side-show-close-button');
    SIDE_TEMPLATE_BUTTON = document.getElementById('side-footer-template-button');
    SIDE_BUTTONS = document.getElementById('side-footer-buttons');
}
