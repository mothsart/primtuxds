"use strict";

export function prefill(constantes, content) {
    $(constantes.SIDE_CONTENT).trumbowyg({
        lang: 'fr',
        btns: [
            ['viewHTML'],
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['base64'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat']
        ]
    });

    let _title = content.title;
    if (!content.title)
        _title = '';
    constantes.SIDE_TITLE.value = _title;

    let _content = '';
    if (content.content)
        _content = content.content;
    constantes.SIDE_CONTENT.innerHTML = _content;
    constantes.SIDE_CLOSE_BUTTON.checked = content.show_close_button;
    _apply_side_footer(constantes, content.footer);
}

export function preview(constantes, component, content) {
    _apply_modal_title(constantes.TITLE, content.title);
    _apply_modal_content(constantes.BODY, content.content);
    _apply_modal_close_button(constantes.CLOSE, content.show_close_button);
    _apply_modal_footer(constantes, content.footer);
    component.events(constantes);
}

export function apply_component(constantes) {
    _apply_modal_title(constantes.TITLE, constantes.SIDE_TITLE.value);
    _apply_modal_content(constantes.BODY, constantes.SIDE_CONTENT.innerHTML);
    _apply_modal_close_button(constantes.CLOSE, constantes.SIDE_CLOSE_BUTTON.checked);
    constantes.FOOTER.innerHTML = '';
    let footer = {};
    Object.entries(constantes.SIDE_BUTTONS.getElementsByTagName('input')).forEach(function (element) {
        footer[element[1].previousSibling.previousSibling.innerText] = {
            'title': element[1].value
        };
    });
    _apply_modal_footer(constantes, footer);
}

function _apply_modal_title(modal_title, modalTitleValue) {
    modal_title.innerText = modalTitleValue;
    showHide(modal_title, modalTitleValue);
}

function _apply_modal_content(modal_body, content) {
    let _content = '';
    if (content)
        _content = content;
    modal_body.innerHTML = _content;
}

function _apply_modal_close_button(close_button, showCloseButton) {
    showHide(close_button, showCloseButton);
}

function _apply_side_footer(constantes, footer) {
    if (!footer)
        return
    Object.entries(footer).map(function (entry) {
        constantes.SIDE_BUTTONS.innerHTML += render(
            constantes.SIDE_TEMPLATE_BUTTON,
            {
                'footer_button_name': entry[0],
                'footer_button_value': entry[1]['title'],
                'footer_button_callback': entry[1]['callback']
            }
        );
    });
}

function _apply_modal_footer(constantes, footer) {
    if (!footer)
        return
    Object.entries(footer).map(function (entry) {
        constantes.FOOTER.innerHTML += render(
            constantes.FOOTER_BUTTON_TEMPLATE,
            {
                'footer_button': entry[1]['title'],
                'footer_button_callback': entry[1]['callback']
            }
        );
    });
}
