"use strict";

function _import(component_name, toml_stream) {
    const ressources = [
        import('./components/'+ component_name + '/constantes.js'),
        import('./components/'+ component_name + '/component.js'),
        import('./components/'+ component_name + '/script.js')
    ]
    Promise.all(ressources).then(values => {
        let constantes = values[0];
        let component = values[1];
        let script = values[2];
        
        constantes.init();
        script.prefill(constantes, toml_stream);
        script.preview(constantes, component, toml_stream);
        document.getElementById('side-validate-button')
        .addEventListener(
            "click",
            function() { script.apply_component(constantes); },
            false
        );
    });
}

function load_example(toml_path, component_name) {
    let prefill_component_callback = window['prefill_' + component_name];
    let preview_component_callback = window['preview_' + component_name];

    const ressources = [
        fetch(toml_path),
        fetch('./static/components/'+ component_name + '/component.html'),
        fetch('./static/components/'+ component_name + '/side.html'),
        
    ]
    Promise.all(ressources).then((responses) => {
        Promise.all(responses.map(res => res.text())).then((values) => {
            let toml_stream = marktytoml(values[0])['contain'];
            COMPONENT_COMPONENT.innerHTML = values[1];
            ASIDE_COMPONENT.innerHTML = values[2];
            _import(component_name, toml_stream);
        });
    });
}
