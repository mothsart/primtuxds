# Primtux DS (Design System)

## Objectifs

- Se donner une identité graphique forte et cohérente
- des règles d'UI/UX
- une GUI pour interagir avec des composants web réutilisables
- avoir des bonnes pratiques d'optimisation
